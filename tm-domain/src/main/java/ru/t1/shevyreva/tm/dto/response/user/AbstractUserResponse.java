package ru.t1.shevyreva.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.response.AbstractResponse;
import ru.t1.shevyreva.tm.model.User;

@Getter
@Setter
public class AbstractUserResponse extends AbstractResponse {

    @Nullable
    private User user;

    public AbstractUserResponse(){}

    public AbstractUserResponse(User user) {
        this.user = user;
    }

}
