package ru.t1.shevyreva.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;
import ru.t1.shevyreva.tm.enumerated.Status;

@Setter
@Getter
@NoArgsConstructor
public class ProjectChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private  String id;

    @Nullable
    private Status status;

    public ProjectChangeStatusByIdRequest(@Nullable final String id, @Nullable final Status status) {
        this.id = id;
        this.status = status;
    }

}
