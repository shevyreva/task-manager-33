package ru.t1.shevyreva.tm.dto.request.user;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
public class UserLockRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserLockRequest(@Nullable final String login) {
        this.login = login;
    }

}
