package ru.t1.shevyreva.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.data.*;
import ru.t1.shevyreva.tm.dto.response.data.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint extends IEndpoint{

    @NotNull
    String NAME =  "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";


    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(){return newInstance(HOST, PORT);}

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider,  NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port,  NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @NotNull DataBackupLoadResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupLoadRequest request
    );

    DataBackupSaveResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupSaveRequest request
    );

    DataLoadBase64Response loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadBase64Request request
    );

    DataSaveBase64Response saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveBase64Request request
    );

    DataLoadBinaryResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadBinaryRequest request
    );

    DataSaveBinaryResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveBinaryRequest request
    );

    DataLoadJsonFasterXmlResponse loadJsonDataFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadJsonFasterXmlRequest request
    );

    DataLoadJsonJaxBResponse loadJsonDataJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadJsonJaxBRequest request
    );

    DataSaveJsonFasterXmlResponse saveJsonDataFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveJsonFasterXmlRequest request
    );

    DataSaveJsonJaxBResponse saveJsonDataJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveJsonJaxBRequest request
    );

    DataLoadXmlFasterXmlResponse loadXmlDataFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadXmlFasterXmlRequest request
    );

    DataLoadXmlJaxBResponse loadXmlDataJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadXmlJaxBRequest request
    );

    DataSaveXmlFasterXmlResponse saveXmlDataFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveXmlFasterXmlRequest request
    );

    DataSaveXmlJaxBResponse saveXmlDataJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveXmlJaxBRequest request
    );

    DataLoadYamlFasterXmlResponse loadYamlDataFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataLoadYamlFasterXmlRequest request
    );

    DataSaveYamlFasterXmlResponse saveYamlDataFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataSaveYamlFasterXmlRequest request
    );

}
