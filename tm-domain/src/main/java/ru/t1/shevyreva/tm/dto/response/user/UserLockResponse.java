package ru.t1.shevyreva.tm.dto.response.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.model.User;

public class UserLockResponse extends AbstractUserResponse{

    public UserLockResponse(@NotNull User user) {
        super(user);
    }

}
