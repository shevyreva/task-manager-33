package ru.t1.shevyreva.tm.api.model;

import org.jetbrains.annotations.Nullable;

public interface ICommand {

    @Nullable
    public String getName();

    @Nullable
    public String getDescription();

    @Nullable
    public String getArgument();

    void execute();

}
