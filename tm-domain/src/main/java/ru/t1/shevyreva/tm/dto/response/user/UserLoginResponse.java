package ru.t1.shevyreva.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public class UserLoginResponse extends AbstractResultResponse {

    public UserLoginResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
