package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.IUserOwnedRepository;
import ru.t1.shevyreva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends IService<M> {

    @NotNull
    M add(@Nullable String userId, @Nullable M model);

    void removeAll(@Nullable String userId);

    @NotNull
    M removeOne(@Nullable String userId, @Nullable M model);

    @NotNull
    List<M> findAll(@Nullable String userId);

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator);

    @NotNull
    M findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    M findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Integer getSize(@Nullable String userId);

    @NotNull
    M removeOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    M removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    boolean existsById(@Nullable String userId, @Nullable String id);

}
