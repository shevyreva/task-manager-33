package ru.t1.shevyreva.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.endpoint.IEndpoint;
import ru.t1.shevyreva.tm.api.service.IServiceLocator;
import ru.t1.shevyreva.tm.api.service.IUserService;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.UserNotFoundException;
import ru.t1.shevyreva.tm.exception.system.RequestEmptyException;
import ru.t1.shevyreva.tm.exception.user.AccessDeniedException;
import ru.t1.shevyreva.tm.exception.user.RoleEmptyException;
import ru.t1.shevyreva.tm.model.User;

import javax.jws.WebService;


public abstract class AbstractEndpoint implements IEndpoint {
    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
    }

    protected void check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (request == null) throw new RequestEmptyException();
        if (role == null) throw new RoleEmptyException();
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new ModelNotFoundException();
        @NotNull final IServiceLocator serviceLocator = getServiceLocator();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findOneById(userId);
        if (userId == null) throw new UserNotFoundException();
        @Nullable final Role roleUser = user.getRole();
        if (roleUser != role) throw new AccessDeniedException();
    }

}
