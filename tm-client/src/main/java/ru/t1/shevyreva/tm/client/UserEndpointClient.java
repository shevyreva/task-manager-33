package ru.t1.shevyreva.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.endpoint.IUserClientEndpoint;
import ru.t1.shevyreva.tm.dto.request.user.*;
import ru.t1.shevyreva.tm.dto.response.user.*;

@NoArgsConstructor
public class UserEndpointClient extends AbstractEndpointClient implements IUserClientEndpoint {

    public UserEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @Override
    public @NotNull UserRegistryResponse registry(@NotNull final UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @Override
    public @NotNull UserChangePasswordResponse changePassword(@NotNull final UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @Override
    public @NotNull UserUpdateProfileResponse updateProfile(@NotNull final UserUpdateProfileRequest request) {
        return call(request, UserUpdateProfileResponse.class);
    }

    @Override
    public @NotNull UserLockResponse lockUser(@NotNull final UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @Override
    public @NotNull UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

    @Override
    public @NotNull UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

}
