package ru.t1.shevyreva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private final String DESCRIPTION = "Change password.";

    @NotNull
    private final String NAME = "user-change-password";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("Enter new password:");
        @NotNull final String password = TerminalUtil.nextLine();
        getUserEndpoint().changePassword(new UserChangePasswordRequest(password));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
