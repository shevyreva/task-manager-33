package ru.t1.shevyreva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.task.TaskGetByIndexRequest;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-show-by-index";

    @NotNull
    private final String DESCRIPTION = "Show task by Index.";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW BY INDEX]");
        System.out.println("Enter Index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Task task = getTaskEndpoint().getTaskByIndex(new TaskGetByIndexRequest(index)).getTask();
        showTask(task);
    }

}
