package ru.t1.shevyreva.tm.command.system;

import org.jetbrains.annotations.NotNull;

public class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    private final String DESCRIPTION = "Show program version.";

    @NotNull
    private final String NAME = "version";

    @NotNull
    private final String ARGUMENT = "-v";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(getPropertyService().getApplicationVersion());
    }

}
