package ru.t1.shevyreva.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.data.DataSaveJsonJaxBRequest;
import ru.t1.shevyreva.tm.enumerated.Role;

public class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Save Data to json file.";

    @NotNull
    private final String NAME = "data-save-json-jaxb";

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @SneakyThrows
    @Override
    public @Nullable void execute() {
        System.out.println("[DATA SAVE JSON]");
        getDomainEndpoint().saveJsonDataJaxB(new DataSaveJsonJaxBRequest());
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
