package ru.t1.shevyreva.tm.api.endpoint;

public interface IUserClientEndpoint extends IEndpointClient, IUserEndpoint {
}
