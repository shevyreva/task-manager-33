package ru.t1.shevyreva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Create new project.";

    @NotNull
    private final String NAME = "project-create";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Enter name: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description: ");
        @NotNull final String description = TerminalUtil.nextLine();
        getProjectEndpoint().createProject(new ProjectCreateRequest(name, description));
    }

}
