package ru.t1.shevyreva.tm.api.endpoint;

public interface IProjectClientEndpoint extends IEndpointClient, IProjectEndpoint {
}
