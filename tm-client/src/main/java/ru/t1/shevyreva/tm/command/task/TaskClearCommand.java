package ru.t1.shevyreva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.task.TaskClearRequest;

public class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Clear task.";

    @NotNull
    private final String NAME = "task-clear";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        getTaskEndpoint().clearTask(new TaskClearRequest());
    }

}
