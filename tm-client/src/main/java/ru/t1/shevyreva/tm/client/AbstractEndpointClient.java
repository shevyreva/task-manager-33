package ru.t1.shevyreva.tm.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.endpoint.IEndpointClient;
import ru.t1.shevyreva.tm.dto.response.ApplicationErrorResponse;

import java.io.*;
import java.net.Socket;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpointClient implements IEndpointClient {

    @NotNull
    public Socket socket;
    @NotNull
    private String host = "localhost";
    @NotNull
    private Integer port = 6060;

    public AbstractEndpointClient(@NotNull final AbstractEndpointClient client) {
        this.host = client.host;
        this.port = client.port;
        this.socket = client.socket;
    }

    @NotNull
    @SneakyThrows
    protected <T> T call(@NotNull final Object data, @NotNull final Class<T> clazz) {
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        if (result instanceof ApplicationErrorResponse) {
            @NotNull final ApplicationErrorResponse response = (ApplicationErrorResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    @NotNull
    @SneakyThrows
    private ObjectOutputStream getObjectOutputStream(){
        return new ObjectOutputStream(getOutputStream());
    }

    @NotNull
    @SneakyThrows
    private ObjectInputStream getObjectInputStream(){
        return new ObjectInputStream(getInputStream());
    }

    @NotNull
    @SneakyThrows
    private OutputStream getOutputStream(){
        return socket.getOutputStream();
    }

    @NotNull
    @SneakyThrows
    private InputStream getInputStream(){
        return socket.getInputStream();
    }

    @Override
    @SneakyThrows
    public void connect() {
        socket = new Socket(host, port);
    }

    @Override
    @SneakyThrows
    public void disconnect() {
        socket.close();
    }

}
