package ru.t1.shevyreva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.task.TaskRemoveByIdRequest;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Remove task by Id.";

    @NotNull
    private final String NAME = "task-remove-by-id";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE BY ID]");
        System.out.println("Enter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        getTaskEndpoint().removeTaskById(new TaskRemoveByIdRequest(id));
    }

}
