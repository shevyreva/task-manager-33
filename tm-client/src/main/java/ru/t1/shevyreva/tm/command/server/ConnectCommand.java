package ru.t1.shevyreva.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.command.AbstractCommand;
import ru.t1.shevyreva.tm.enumerated.Role;

import java.net.Socket;

public class ConnectCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "connect";
    @NotNull
    private final String DESCRIPTION = "Connect to the server.";

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @Nullable void execute() {
        getServiceLocator().getAuthEndpoint().connect();
        @NotNull final Socket socket = getServiceLocator().getAuthEndpoint().getSocket();
        getServiceLocator().getProjectEndpoint().setSocket(socket);
        getServiceLocator().getTaskEndpoint().setSocket(socket);
        getServiceLocator().getUserEndpoint().setSocket(socket);
        getServiceLocator().getDomainEndpoint().setSocket(socket);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return null;
    }

}
