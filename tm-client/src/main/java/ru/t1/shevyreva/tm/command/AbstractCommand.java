package ru.t1.shevyreva.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.model.ICommand;
import ru.t1.shevyreva.tm.api.service.IServiceLocator;
import ru.t1.shevyreva.tm.enumerated.Role;


@Setter
@Getter
public abstract class AbstractCommand implements ICommand {

    @Nullable
    protected IServiceLocator serviceLocator;

    @Nullable
    public abstract String getName();

    @Nullable
    public abstract String getDescription();

    @Nullable
    public abstract String getArgument();

    @Nullable
    public abstract void execute();

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        @NotNull String displayName = "";
        if (name != null && !name.isEmpty()) displayName += name;
        if (argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if (description != null && !description.isEmpty()) displayName += ": " + description;
        return displayName;
    }

}
