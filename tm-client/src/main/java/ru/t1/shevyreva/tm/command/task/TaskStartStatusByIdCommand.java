package ru.t1.shevyreva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class TaskStartStatusByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Task start by Id.";

    @NotNull
    private final String NAME = "task-start-by-id";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("Enter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        getTaskEndpoint().changeTaskStatusById(new TaskChangeStatusByIdRequest(id, Status.IN_PROGRESS));
    }

}
